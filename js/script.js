'use strict';

const arr = ["travel", "hello", "eat", "ski", "lift"];
const symbolLength = arr.filter((value) => value.length >3);
let result = symbolLength.length
console.log(result);



const arrPeople = [
    {name: "Іван", age: 25, sex: "чоловік"},
    {name: "Оксана", age: 18, sex: "жінка"},
    {name: "Олег", age: 34, sex: "чоловік"},
    {name: "Юля", age: 27, sex: "жінка"},
];
console.log(arrPeople);
function filterPeople(arrPeople, sex) {
    const filterMan = arrPeople.filter(
        (person) => person.sex === "чоловік");
    return filterMan;
}
console.log(filterPeople(arrPeople));




function filterBy(array, dateType) {
    const filterType = array.filter(
        (element) => typeof(element) !== dateType
    );
    return filterType;
};
console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));